type Position = {
    x: number,
    y: number,
}
let input_str: string = await Bun.file("./input.txt").text();
let lines: string[] = input_str.split("\n") 
let width: number = lines[0].length
let height: number = lines.length
let garden: Set<number> = new Set();
let start_point: number = -1;
for (let [y, line] of input_str.split("\n").entries()) {
  for (let x = 0; x < line.length; ++x) {
    let tile = line.charAt(x);
    if (tile == ".") {
      garden.add(y * width + x);
    } else if (tile == "S") {
      start_point = y * width + x;
      garden.add(y * width + x)
    }
  }
}
const MAX_STEPS = 64;
let visited: Set<number> = new Set()
visited.add(start_point)
for (let i = 1; i <= MAX_STEPS; ++i) {
    let new_visited: Set<number> = new Set()
    for (let old_pos of visited) {
        let x = old_pos % width;
        let y = (old_pos / width) >> 0;
        let candidates: Position[] = [{x, y: y-1}, {x, y: y+1}, {x: x-1, y}, {x: x+1, y}];
        for (let candidate of candidates) {
            const candidate_pos = candidate.y * width + candidate.x;
            if (garden.has(candidate_pos)) {
                new_visited.add(candidate_pos);
            }
        }
    }
    visited = new_visited
}
console.log(visited.size)
