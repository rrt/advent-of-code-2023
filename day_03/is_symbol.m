function ret = is_symbol(c)
    if (c != 46) && ((c < 48 ) || (c > 57))
        ret = true;
    else
        ret = false;
    end
end