with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
procedure Day_12 is
   type SpringCondition is (Unknown, Operational, Damaged);
   type SpringArray is array(Integer range <>) of SpringCondition;
   type NumberSeq is  array(Integer range<>) of Integer;
   function ArraySum(arr: NumberSeq) return Integer is
   s : Integer := 0;
   begin
      for i in arr'Range loop
         s := s + arr(i);
      end loop;
      return s;
   end ArraySum;
   function CharacterToSpring(ch: Character) return SpringCondition is
   begin
      if ch = '.' then
         return Operational;
      else
         if ch = '?' then
            return Unknown;

         else
            return Damaged;
         end if;
      end if;
   end CharacterToSpring;
   function StringToSprings(s: String) return SpringArray is
      ReturnSprings: SpringArray( 1..s'Length);
   begin
      for i in s'First..s'Last loop
         ReturnSprings(i) := CharacterToSpring(s(i));
      end loop;
      return ReturnSprings;
   end StringToSprings;
   function FiveSprings(springs: SpringArray) return SpringArray is
      fivex: SpringArray( 1 .. (5 * springs'Length) + 4);
      index: Integer := 1;
   begin
      for j in 0..4 loop
         for i in springs'Range loop
            fivex(index) := springs(i);
            index := index + 1;
         end loop;
         if j /= 4 then
            fivex(index) := Unknown;
            index := index + 1;
         end if;
      end loop;
      return fivex;
   end FiveSprings;
   function FiveSeqs(nums: NumberSeq) return NumberSeq is
      fivex: NumberSeq( 1 .. (5 * nums'Length));
   begin
      for i in nums'Range loop
         for j in 0..4 loop
            fivex(nums'Length * j + i) := nums(i);
         end loop;
      end loop;
      return fivex;
   end FiveSeqs;
   function CheckDamaged(spring_array: SpringArray) return Boolean is
   begin
      for i in spring_array'Range loop
         if spring_array(i) = Operational then
            return False;
         end if;
      end loop;
      return true;
   end CheckDamaged;
   function CheckOperational(spring_array: SpringArray) return Boolean is
   begin
      for i in spring_array'Range loop
         if spring_array(i) = Damaged then
            return False;
         end if;
      end loop;
      return True;
   end CheckOperational;
   function HowManyMatches(spring_array: SpringArray; number_seq: NumberSeq) return Long_Integer is
      best : array(1 .. spring_array'Length) of Long_Integer;
   begin
      -- Compute best for last number.
      declare
         last_number : Integer := number_seq(number_seq'Last);
      begin
         for start_index in spring_array'Range loop
            if start_index + number_seq(number_seq'Last) - 1 > spring_array'Last then
               best(start_index) := 0;
            else
               if CheckDamaged(spring_array(start_index..start_index + last_number - 1)) and (((start_index + last_number - 1) = spring_array'Last) or CheckOperational(spring_array(start_index + last_number .. spring_array'Last))) then
                  best(start_index) := 1;
               else
                  best(start_index) := 0;
               end if;
            end if;
         end loop;
      end;
      declare
         new_best : array(1 .. spring_array'Length) of Long_Integer;
      begin
         for number_index in reverse number_seq'First .. number_seq'Last - 1 loop
            declare
               number : Integer := number_seq(number_index);
            begin
               for start_index in spring_array'Range loop
                  if (start_index + number + 1) > spring_array'Last then
                     new_best(start_index) := 0;
                  else if CheckDamaged(spring_array(start_index .. start_index + number - 1)) then
                        new_best(start_index) := 0;
                        declare
                           second_index : Integer := start_index + number;
                        begin
                           while second_index < spring_array'Last and then spring_array(second_index) /= Damaged loop
                              new_best(start_index) := new_best(start_index) + best(second_index + 1);
                              second_index := second_index + 1;
                           end loop;
                        end;
                     else
                        new_best(start_index) := 0;
                     end if;
                  end if;
               end loop;
            end;
            for idx in best'Range loop
               best(idx) := new_best(idx);
            end loop;
         end loop;
      end;
      declare
         start_index : Integer := spring_array'First;
         total : Long_Integer := best(spring_array'First);
      begin
         while start_index < spring_array'Last and then spring_array(start_index) /= Damaged loop
            total := total + best(start_index + 1);
            start_index := start_index + 1;
         end loop;
         return total;
      end;
   end HowManyMatches;
   function FirstIndexOf(s: String; ch: Character) return Integer is
   begin
      for i in s'First..s'Last loop
         if s(i) = ch then
            return i;
         end if;
      end loop;
      return -1;
   end FirstIndexOf;
   function CountChars(s: String; ch: Character) return Integer is
   count : Integer := 0;
   index : Integer := FirstIndexOf(s, ch);
   begin
      while index > 0 loop
         count := count + 1;
         index := FirstIndexOf(s(index + 1 .. s'Last), ch);
      end loop;
      return count;
   end CountChars;
   function GetNumbers(s: String) return NumberSeq is
      size: Integer := 1 + CountChars(s, ',');
      nums: NumberSeq(1 .. size);
      index: Integer := s'First;
   begin
      for i in 1..size loop
         declare
            number : Integer := 0;
         begin
            while index <= s'Last and then s(index) /= ',' loop
               number := number * 10 + Character'Pos(s(index)) - Character'Pos('0');
               index := index + 1;
            end loop;
            index := index + 1;
            nums(i) := number;
         end;
      end loop;
      return nums;
   end GetNumbers;
   procedure PartOne(filename: String) is
   F         : File_Type;
   S         : Long_Integer := 0;
   begin
      Open(F, In_File, filename);
      while not End_Of_File (F) loop
         declare
            Line : String := Get_Line(F);
            FirstSpace : Integer := FirstIndexOf(Line, ' ');
            Springs : SpringArray := StringToSprings(Line(1 .. FirstSpace - 1));
            NumSeq : NumberSeq := GetNumbers(Line(FirstSpace + 1 .. Line'Last));
         begin
            S := S + HowManyMatches(Springs, NumSeq);
         end;
      end loop;
      Put_Line(Long_Integer'Image(S));
      Close(F);
   end PartOne;
   procedure PartTwo(filename: String) is
   F         : File_Type;
   S         : Long_Integer := 0;
   Line_idx  : Integer := 1;
   begin
      Open(F, In_File, filename);
      while not End_Of_File (F) loop
         declare
            Line : String := Get_Line(F);
            FirstSpace : Integer := FirstIndexOf(Line, ' ');
            Springs : SpringArray := StringToSprings(Line(1 .. FirstSpace - 1));
            NumSeq : NumberSeq := GetNumbers(Line(FirstSpace + 1 .. Line'Last));
            Springsx5 : SpringArray := FiveSprings(Springs);
            NumSeqx5 : NumberSeq := FiveSeqs(NumSeq);
            TotalSum : Integer := ArraySum(NumSeqx5);
         begin
            --  Put_Line("Line " & Integer'Image(Line_idx));
            S := S + HowManyMatches(Springsx5, NumSeqx5);
            Line_idx := Line_idx + 1;
         end;
      end loop;
      Put_Line(Long_Integer'Image(S));
      Close(F);
   end PartTwo;
begin
   PartOne("input.txt");
   PartTwo("input.txt");
end Day_12;
