const std = @import("std");

const RockType = enum {
    Rolling,
    Static,
};

const Rock = struct { x: usize, y: usize, t: RockType };

const Board = struct {
    min_x: usize,
    max_x: usize,
    min_y: usize,
    max_y: usize,
    rocks: std.ArrayList(std.ArrayList(Rock)),

    fn move_north(self: *Board, allocator: std.mem.Allocator) !void {
        for (self.min_x..self.max_x + 1) |x| {
            var column = std.ArrayList(Rock).init(allocator);
            defer column.deinit();
            var next_available_y = self.min_y;
            for (self.rocks.items[x - 1].items) |rock| {
                switch (rock.t) {
                    RockType.Rolling => {
                        try column.append(Rock{ .x = x, .y = next_available_y, .t = RockType.Rolling });
                        next_available_y += 1;
                    },
                    RockType.Static => {
                        next_available_y = rock.y + 1;
                        try column.append(Rock{ .x = rock.x, .y = rock.y, .t = RockType.Static });
                    },
                }
            }
            for (self.rocks.items[x - 1].items, 0..) |_, idx| {
                self.rocks.items[x - 1].items[idx] = column.items[idx];
            }
        }
    }

    fn move_south(self: *Board, allocator: std.mem.Allocator) !void {
        for (self.min_x..self.max_x + 1) |x| {
            var column = std.ArrayList(Rock).init(allocator);
            defer column.deinit();
            var next_available_y = self.max_y;
            var rev_idx = self.rocks.items[x - self.min_x].items.len;
            while (rev_idx > 0) : (rev_idx -= 1) {
                const rock = self.rocks.items[x - self.min_x].items[rev_idx - 1];
                switch (rock.t) {
                    RockType.Rolling => {
                        try column.append(Rock{ .x = x, .y = next_available_y, .t = RockType.Rolling });
                        next_available_y -= 1;
                    },
                    RockType.Static => {
                        next_available_y = rock.y - 1;
                        try column.append(Rock{ .x = rock.x, .y = rock.y, .t = RockType.Static });
                    },
                }
            }
            for (self.rocks.items[x - self.min_x].items, 0..) |_, idx| {
                self.rocks.items[x - self.min_x].items[idx] = column.items[idx];
            }
        }
    }
    fn move_west(self: *Board, allocator: std.mem.Allocator) !void {
        var new_rocks = std.ArrayList(std.ArrayList(Rock)).init(allocator);
        defer new_rocks.deinit();
        for (self.min_x..self.max_x + 1) |_| {
            try new_rocks.append(std.ArrayList(Rock).init(allocator));
        }
        defer for (new_rocks.items) |column| {
            column.deinit();
        };
        for (self.min_y..self.max_y + 1) |y| {
            var old_row = std.ArrayList(Rock).init(allocator);
            defer old_row.deinit();
            for (self.min_x..self.max_x + 1) |x| {
                for (self.rocks.items[x - self.min_x].items) |rock| {
                    if (rock.y == y) {
                        try old_row.append(rock);
                    }
                }
            }
            var next_available_x = self.min_x;
            for (old_row.items) |rock| {
                switch (rock.t) {
                    RockType.Rolling => {
                        try new_rocks.items[next_available_x - self.min_x].append(Rock{ .x = next_available_x, .y = y, .t = RockType.Rolling });
                        next_available_x += 1;
                    },
                    RockType.Static => {
                        try new_rocks.items[rock.x - self.min_x].append(Rock{ .x = rock.x, .y = y, .t = RockType.Static });
                        next_available_x = rock.x + 1;
                    },
                }
            }
        }
        for (self.min_x..self.max_x + 1) |x| {
            self.rocks.items[x - self.min_x].clearRetainingCapacity();
            for (new_rocks.items[x - self.min_x].items) |rock| {
                try self.rocks.items[x - self.min_x].append(rock);
            }
        }
    }

    fn move_east(self: *Board, allocator: std.mem.Allocator) !void {
        var new_rocks = std.ArrayList(std.ArrayList(Rock)).init(allocator);
        defer new_rocks.deinit();
        for (self.min_x..self.max_x + 1) |_| {
            try new_rocks.append(std.ArrayList(Rock).init(allocator));
        }
        defer for (new_rocks.items) |column| {
            column.deinit();
        };
        for (self.min_y..self.max_y + 1) |y| {
            var old_row = std.ArrayList(Rock).init(allocator);
            defer old_row.deinit();
            for (self.min_x..self.max_x + 1) |x| {
                for (self.rocks.items[x - self.min_x].items) |rock| {
                    if (rock.y == y) {
                        try old_row.append(rock);
                    }
                }
            }
            var next_available_x = self.max_x;
            var rev_idx = old_row.items.len;

            while (rev_idx > 0) : (rev_idx -= 1) {
                const rock = old_row.items[rev_idx - 1];
                switch (rock.t) {
                    RockType.Rolling => {
                        try new_rocks.items[next_available_x - self.min_x].append(Rock{ .x = next_available_x, .y = y, .t = RockType.Rolling });
                        next_available_x -= 1;
                    },
                    RockType.Static => {
                        try new_rocks.items[rock.x - self.min_x].append(Rock{ .x = rock.x, .y = y, .t = RockType.Static });
                        next_available_x = rock.x - 1;
                    },
                }
            }
        }
        for (self.min_x..self.max_x + 1) |x| {
            self.rocks.items[x - self.min_x].clearRetainingCapacity();
            for (new_rocks.items[x - self.min_x].items) |rock| {
                try self.rocks.items[x - self.min_x].append(rock);
            }
        }
    }

    fn cycle(self: *Board, allocator: std.mem.Allocator) !void {
        try self.move_north(allocator);
        try self.move_west(allocator);
        try self.move_south(allocator);
        try self.move_east(allocator);
    }
    fn print(self: *Board) !void {
        const stdout = std.io.getStdOut().writer();
        for (self.min_y..self.max_y + 1) |y| {
            for (self.min_x..self.max_x + 1) |x| {
                var printed = false;
                for (self.rocks.items[x - self.min_x].items) |rock| {
                    if (rock.y == y) {
                        switch (rock.t) {
                            RockType.Rolling => {
                                try stdout.print("O", .{});
                                printed = true;
                            },
                            RockType.Static => {
                                try stdout.print("#", .{});
                                printed = true;
                            },
                        }
                    }
                }
                if (!printed) {
                    try stdout.print(".", .{});
                }
            }
            try stdout.print("\n", .{});
        }
    }
    fn compute_load(self: *const Board) usize {
        var sum: usize = 0;
        for (self.rocks.items) |rock_column| {
            for (rock_column.items) |rock| {
                if (rock.t == RockType.Rolling) {
                    const single_load = self.max_y - (rock.y - self.min_y);
                    sum += single_load;
                }
            }
        }
        return sum;
    }
    // This is not meant to be in any way a good choice; it's purely vibes based.
    fn board_hash(self: *const Board) usize {
        var h: usize = 69420;
        for (self.rocks.items) |column| {
            for (column.items) |rock| {
                if (rock.t == RockType.Rolling) {
                    h = (((h * 132049) % 24036583) + ((rock.x * 25964951) % 24036583)) % 24036583;
                }
            }
        }
        return h;
    }
};

fn read_board(allocator: std.mem.Allocator, filename: []const u8) !Board {
    var y: usize = 0;
    var min_y: usize = 100000000;
    var min_x: usize = 100000000;
    var max_x: usize = 0;
    var max_y: usize = 0;

    const file = try std.fs.cwd().openFile(
        filename,
        .{},
    );
    defer file.close();

    var rocks = std.ArrayList(std.ArrayList(Rock)).init(allocator);

    while (true) {
        const contents = file.reader().readUntilDelimiterAlloc(allocator, '\n', 4096) catch {
            break;
        };
        defer allocator.free(contents);
        if (contents.len == 0) {
            break;
        }
        y += 1;
        if (y > max_y) {
            max_y = y;
        }
        if (y < min_y) {
            min_y = y;
        }
        var x: usize = 0;
        for (contents) |ch| {
            x += 1;
            if (x < min_x) {
                min_x = x;
            }
            if (x > max_x) {
                max_x = x;
            }
            while (rocks.items.len < x) {
                try rocks.append(std.ArrayList(Rock).init(allocator));
            }
            switch (ch) {
                'O' => {
                    try rocks.items[x - 1].append(Rock{ .x = x, .y = y, .t = RockType.Rolling });
                },
                '#' => {
                    try rocks.items[x - 1].append(Rock{ .x = x, .y = y, .t = RockType.Static });
                },
                else => {},
            }
        }
    }
    return Board{
        .max_x = max_x,
        .min_x = min_x,
        .min_y = min_y,
        .max_y = max_y,
        .rocks = rocks,
    };
}

pub fn main() !void {
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gpa.allocator();
    var board = try read_board(allocator, "input.txt");
    try board.move_north(allocator);
    const load = board.compute_load();
    var history = std.ArrayList(usize).init(allocator);
    var last_used = std.AutoHashMap(usize, usize).init(allocator);
    var start: usize = 0;
    var cycle_length: usize = 0;
    try stdout.print("{}\n", .{load});
    while (true) {
        try board.cycle(allocator);
        const new_load = board.compute_load();
        try history.append(new_load);
        const bh = board.board_hash();
        if (last_used.contains(bh)) {
            start = last_used.get(bh) orelse 0;
            cycle_length = (history.items.len - 1) - start;
            break;
        } else {
            try last_used.put(bh, history.items.len - 1);
        }
    }
    const idx = (1000000000 - start - 1) % cycle_length + start;
    try stdout.print("{}\n", .{history.items[idx]});

    try bw.flush();
}
