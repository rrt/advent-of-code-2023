local filename = "input.txt"

Set = {}

function Set:new()
    local t = setmetatable({}, {__index = Set})
    t.data = {}
    return t
end
function Set:insert(val)
    assert(val.index ~= nil)
    self.data[val.index] = val
end
function Set:remove(val)
    assert(val.index ~= nil)
    self.data[val.index] = nil
end
function Set:contains(val)
    assert(val.index ~= nil)
    return self.data[val.index] ~= nil
end

TilePos = { }
function TilePos:new(x, y)
    local t = setmetatable({}, {__index = TilePos})
    t.x = x
    t.y = y
    t.index = x .. "_" .. y
    return t
end

Tile = { }
function Tile:new(x, y, kind)
    local t = setmetatable({}, {__index = Tile})
    t.pos = TilePos:new(x, y)
    t.valid_neighbours = {}
    t.real_neighbours = {}
    t.kind = kind
    if kind == "S" then
        table.insert(t.valid_neighbours, TilePos:new(x - 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x + 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x, y - 1))
        table.insert(t.valid_neighbours, TilePos:new(x, y + 1))
    elseif kind == "|" then
        table.insert(t.valid_neighbours, TilePos:new(x, y - 1))
        table.insert(t.valid_neighbours, TilePos:new(x, y + 1))
    elseif kind == "-" then
        table.insert(t.valid_neighbours, TilePos:new(x - 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x + 1, y))
    elseif kind == "L" then
        table.insert(t.valid_neighbours, TilePos:new(x + 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x, y - 1))
    elseif kind == "J" then
        table.insert(t.valid_neighbours, TilePos:new(x - 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x, y - 1))
    elseif kind == "7" then
        table.insert(t.valid_neighbours, TilePos:new(x - 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x, y + 1))
    elseif kind == "F" then
        table.insert(t.valid_neighbours, TilePos:new(x + 1, y))
        table.insert(t.valid_neighbours, TilePos:new(x, y + 1))
    end
    return t
end

function Tile:can_have_neighbour_at_pos(neighbour_pos)
    for _, potential_neighbour_pos in pairs(self.valid_neighbours) do
        if potential_neighbour_pos.x == neighbour_pos.x and potential_neighbour_pos.y == neighbour_pos.y then
            return true
        end
    end
    return false
end

function Tile:add_neighbour(at_pos)
    table.insert(self.real_neighbours, at_pos)
end


Graph = {}
function Graph:new()
    local t = setmetatable({}, {__index = Graph})
    t.max_x = -1000
    t.min_x = 1000000000
    t.max_y = -1000
    t.min_y = 1000000000
    t.adjacency_list = {}
    return t
end

function Graph:is_border_pos(pos)
    if pos.x <= self.min_x or pos.x >= self.max_x or pos.y <= self.min_y or pos.y >= self.max_y then
        return true
    else
        return false
    end
end
function Graph:add_tile(tile)
    if tile.pos.x > self.max_x then
        self.max_x = tile.pos.x
    end
    if tile.pos.x < self.min_x then
        self.min_x = tile.pos.x
    end

    if tile.pos.y > self.max_y then
        self.max_y = tile.pos.y
    end
    if tile.pos.y < self.min_y then
        self.min_y = tile.pos.y
    end
    self.adjacency_list[tile.pos.index] = tile
end
function Graph:get_tile(at_pos)
    return self.adjacency_list[at_pos.index]
end
function Graph:create_adjacency_list()
    for _, tile in pairs(self.adjacency_list) do
        for _, neighbour_pos in pairs(tile.valid_neighbours) do
            local neighbour = self:get_tile(neighbour_pos)
            if neighbour ~= nil and neighbour:can_have_neighbour_at_pos(tile.pos) then
                tile:add_neighbour(neighbour_pos)
            end
        end
    end
end

Queue = {}
function Queue:new()
    local t = setmetatable({}, { __index = Queue })
    t.next_index = 1
    t.curr_index = 1
    t.my_table = {}
    return t
end
function Queue:push(val)
    local index = self.next_index
    self.my_table[index] = val
    self.next_index = index + 1
end
function Queue:pop()
    assert(self.next_index > self.curr_index, "Trying to pop from an empty queue")
    local index = self.curr_index
    local val = self.my_table[index]
    self.my_table[index] = nil
    self.curr_index = index + 1
    return val
end
function Queue:is_empty()
    return self.curr_index == self.next_index
end


-- main

local start_tile = nil

-- Map from position to Tile
local graph = Graph:new()
local y_index = 0
local tiles = {}

local max_x = 0
local max_y = 0

-- Read tiles
for line in io.lines(filename) do
    y_index = y_index + 1
    max_x = line:len()
    for x_index=1,line:len() do
        local tile_kind = string.sub(line, x_index, x_index)
        if tile_kind == "S" then
            start_tile = Tile:new(x_index, y_index, "S")
            graph:add_tile(start_tile)
            table.insert(tiles, start_tile)
        elseif tile_kind ~= "." then
            local graph_tile = Tile:new(x_index, y_index, tile_kind)
            graph:add_tile(graph_tile)
            table.insert(tiles, graph_tile)
        end
    end
end

max_y = y_index

graph:create_adjacency_list()

-- Part one
local q = Queue:new()

BFSNode = {}
function BFSNode:new(at_pos, dist)
    local t = setmetatable({}, {__index = BFSNode})
    t.tile = at_pos
    t.dist = dist
    t.index = at_pos.index
    return t
end

assert(start_tile ~= nil, "Must have a start tile")
local start_node = BFSNode:new(start_tile.pos, 0)
local loop_tiles = Set:new()
loop_tiles:insert(start_tile.pos)
q:push(start_node)
local max_dist = -10
while not q:is_empty() do
    --@type BFSNode
    local node = q:pop()
    --@type Tile
    local tile = graph:get_tile(node.tile)
    assert(tile ~= nil, "WTF")
    local dist = node.dist
    if dist > max_dist then
        max_dist = dist
    end
    for _, neighbour in pairs(tile.real_neighbours) do
        if not loop_tiles:contains(neighbour) then
            local new_node = BFSNode:new(neighbour, dist + 1)
            loop_tiles:insert(neighbour)
            q:push(new_node)
        end
    end
end

print (max_dist)

-- Part two
-- Expand the graph to make gaps
local expanded_graph = Graph:new()
local real_nodes = Set:new()
for y=1,max_y do
    for x=1,max_x do
        local pos = TilePos:new(x, y)
        local base_x = (x - 1) * 3
        local base_y = (y - 1) * 3
        -- This tile is part of the original loop
        if loop_tiles:contains(pos) then
            local tile = graph:get_tile(pos)
            local kind = tile.kind
            if kind == "S" then
                --              # # #
                --   S       => # S #
                --              # # #                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "7"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "#"))
            elseif kind == "|" then
                --              S # S
                --   |       => S # S
                --              S # S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            elseif kind == "-" then
                --              S S S
                --   -       => # # #
                --              S S S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            elseif kind == "L" then
                --              S # S
                --   L       => S # #
                --              S S S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            elseif kind == "J" then
                --              S # S
                --   J       => # # S
                --              S S S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            elseif kind == "7" then
                --              S S S
                --   7       => # # S
                --              S # S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            elseif kind == "F" then
                --              S S S
                --   F       => S # #
                --              S # S                
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
                expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "#"))
                expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
            end
        else
            real_nodes:insert(TilePos:new(base_x + 1, base_y + 1))
            --          S S S
            --   S   => S S S
            --          S S S                
            expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 0, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 0, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 0, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 1, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 1, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 1, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 0, base_y + 2, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 1, base_y + 2, "S"))
            expanded_graph:add_tile(Tile:new(base_x + 2, base_y + 2, "S"))
        end
    end
end
expanded_graph:create_adjacency_list()

local total_enclosed = 0
local visited = Set:new()
for _, start_node in pairs(real_nodes.data) do
    if not visited:contains(start_node) then
        local q = Queue:new()
        q:push(start_node)
        visited:insert(start_node)
        local curr_component_real_nodes = 0
        local is_enclosed = true
        local visited_in_component = Set:new()
        visited_in_component:insert(start_node)
        while not q:is_empty() do
            local curr_node = q:pop()
            if real_nodes:contains(curr_node) then
                curr_component_real_nodes = curr_component_real_nodes + 1
            end
            if expanded_graph:is_border_pos(curr_node) then
                is_enclosed = false
            end
            local tile = expanded_graph:get_tile(curr_node)
            if tile ~= nil then
                for _, neighbour_pos in pairs(tile.real_neighbours) do
                    if not visited_in_component:contains(neighbour_pos) then
                        q:push(neighbour_pos)
                        visited_in_component:insert(neighbour_pos)
                        visited:insert(neighbour_pos)
                    end
                end
            end
        end
        if is_enclosed then
            total_enclosed = total_enclosed + curr_component_real_nodes
        end
    end
end
print(total_enclosed)

