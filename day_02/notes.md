Yorick documentation is very lacking in samples. I assume it's mostly expected to be understood through 'work of mouth'. Either that, or it's because most websites that use it are unindexed.
For example, the [faq](https://yorick.sourceforge.net/yorickfaq.php) laments that "people often overlook the pr1, swrite, and sread functions as well, which are useful for generating strings from numbers and vice versa", but these functions have very scant and cryptic documentation, and are only ever used once in the whole FAQ.
I'm sure that my frustration is mostly due to trying to put a square peg into a round hole - or, more concretely, to use a scientific data processing language for string processing - but its lack of documentation is warranted.
The problem today was though MUCH easier.
