package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Part struct {
	x int
	m int
	a int
	s int
}

func (p Part) rating() int {
	return p.x + p.m + p.a + p.s
}

type SubRule interface {
	applies(part Part) (CondResult, *string)
}

type Rule []SubRule

type Solution struct {
	admittedX []bool
	admittedM []bool
	admittedA []bool
	admittedS []bool
}

func newFullSolution() Solution {
	x := []bool{}
	m := []bool{}
	a := []bool{}
	s := []bool{}
	for i := 0; i < 4000; i++ {
		x = append(x, true)
		m = append(m, true)
		a = append(a, true)
		s = append(s, true)
	}
	return Solution{
		admittedX: x,
		admittedM: m,
		admittedA: a,
		admittedS: s,
	}
}

func countTrue(vals []bool) int {
	ret := 0
	for _, val := range vals {
		if val {
			ret += 1
		}
	}
	return ret
}

func (s Solution) possibilities() int {
	return countTrue(s.admittedX) * countTrue(s.admittedM) * countTrue(s.admittedA) * countTrue(s.admittedS)
}

type Cond int

const (
	LessThan Cond = iota
	GreaterThan
)

type CondResult int

const (
	False CondResult = iota
	True
	Unknown
)

type Accept struct{}
type Reject struct{}
type Fallthrough struct {
	label string
}
type LhsSelector interface {
	selectPart(part Part) int
	selectConditionTrue(cond Cond, rhs int, sol Solution) Solution
	selectConditionFalse(cond Cond, rhs int, sol Solution) Solution
}

type XSelector struct{}
type MSelector struct{}
type ASelector struct{}
type SSelector struct{}

func copySlice(s []bool) []bool {
	ret := make([]bool, len(s))
	copy(ret, s)
	return ret
}

func (XSelector) selectPart(part Part) int {
	return part.x
}
func (XSelector) selectConditionTrue(cond Cond, rhs int, sol Solution) Solution {
	newX := make([]bool, len(sol.admittedX))
	copy(newX, sol.admittedX)
	if cond == LessThan {
		for i := rhs - 1; i < len(newX); i++ {
			newX[i] = false
		}
	} else {
		for i := 0; i < rhs; i++ {
			newX[i] = false
		}
	}
	return Solution{
		admittedX: newX,
		admittedM: sol.admittedM,
		admittedA: sol.admittedA,
		admittedS: sol.admittedS,
	}
}
func (XSelector) selectConditionFalse(cond Cond, rhs int, sol Solution) Solution {
	newX := make([]bool, len(sol.admittedX))
	copy(newX, sol.admittedX)
	if cond == LessThan {
		for i := 0; i < rhs-1; i++ {
			newX[i] = false
		}
	} else {
		for i := rhs; i < len(newX); i++ {
			newX[i] = false
		}
	}
	return Solution{
		admittedX: newX,
		admittedM: copySlice(sol.admittedM),
		admittedA: copySlice(sol.admittedA),
		admittedS: copySlice(sol.admittedS),
	}
}

func (MSelector) selectPart(part Part) int {
	return part.m
}
func (MSelector) selectConditionTrue(cond Cond, rhs int, sol Solution) Solution {
	newM := make([]bool, len(sol.admittedM))
	copy(newM, sol.admittedM)
	if cond == LessThan {
		for i := rhs - 1; i < len(newM); i++ {
			newM[i] = false
		}
	} else {
		for i := 0; i < rhs; i++ {
			newM[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: newM,
		admittedA: copySlice(sol.admittedA),
		admittedS: copySlice(sol.admittedS),
	}
}
func (MSelector) selectConditionFalse(cond Cond, rhs int, sol Solution) Solution {
	newM := make([]bool, len(sol.admittedM))
	copy(newM, sol.admittedM)
	if cond == LessThan {
		for i := 0; i < rhs-1; i++ {
			newM[i] = false
		}
	} else {
		for i := rhs; i < len(newM); i++ {
			newM[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: newM,
		admittedA: copySlice(sol.admittedA),
		admittedS: copySlice(sol.admittedS),
	}
}

func (ASelector) selectPart(part Part) int {
	return part.a
}
func (ASelector) selectConditionTrue(cond Cond, rhs int, sol Solution) Solution {
	newA := make([]bool, len(sol.admittedA))
	copy(newA, sol.admittedA)
	if cond == LessThan {
		for i := rhs - 1; i < len(newA); i++ {
			newA[i] = false
		}
	} else {
		for i := 0; i < rhs; i++ {
			newA[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: copySlice(sol.admittedM),
		admittedA: newA,
		admittedS: copySlice(sol.admittedS),
	}
}
func (ASelector) selectConditionFalse(cond Cond, rhs int, sol Solution) Solution {
	newA := make([]bool, len(sol.admittedA))
	copy(newA, sol.admittedA)
	if cond == LessThan {
		for i := 0; i < rhs-1; i++ {
			newA[i] = false
		}
	} else {
		for i := rhs; i < len(newA); i++ {
			newA[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: copySlice(sol.admittedM),
		admittedA: newA,
		admittedS: copySlice(sol.admittedS),
	}
}

func (SSelector) selectPart(part Part) int {
	return part.s
}
func (SSelector) selectConditionTrue(cond Cond, rhs int, sol Solution) Solution {
	newS := make([]bool, len(sol.admittedS))
	copy(newS, sol.admittedS)
	if cond == LessThan {
		for i := rhs - 1; i < len(newS); i++ {
			newS[i] = false
		}
	} else {
		for i := 0; i < rhs; i++ {
			newS[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: copySlice(sol.admittedM),
		admittedA: copySlice(sol.admittedA),
		admittedS: newS,
	}
}
func (SSelector) selectConditionFalse(cond Cond, rhs int, sol Solution) Solution {
	newS := make([]bool, len(sol.admittedS))
	copy(newS, sol.admittedS)
	if cond == LessThan {
		for i := 0; i < rhs-1; i++ {
			newS[i] = false
		}
	} else {
		for i := rhs; i < len(newS); i++ {
			newS[i] = false
		}
	}
	return Solution{
		admittedX: copySlice(sol.admittedX),
		admittedM: copySlice(sol.admittedM),
		admittedA: copySlice(sol.admittedA),
		admittedS: newS,
	}
}

type Condition struct {
	t           Cond
	lhsSelector LhsSelector
	rhs         int
	trueLabel   string
}

func (Accept) applies(_ Part) (CondResult, *string) {
	return True, nil
}
func (Reject) applies(_ Part) (CondResult, *string) {
	return False, nil
}
func (a Fallthrough) applies(_ Part) (CondResult, *string) {
	return Unknown, &a.label
}

func parseSubRule(ruleString string) SubRule {
	if ruleString == "A" {
		return Accept{}
	} else if ruleString == "R" {
		return Reject{}
	} else {
		splitContents := strings.Split(ruleString, ":")
		if len(splitContents) == 1 {
			return Fallthrough{
				label: splitContents[0],
			}
		}
		trueLabel := splitContents[1]
		condStr := splitContents[0]
		condType := LessThan
		indexOfOperator := strings.Index(condStr, "<")
		if indexOfOperator == -1 {
			indexOfOperator = strings.Index(condStr, ">")
			condType = GreaterThan
		}
		lhsString := condStr[0:indexOfOperator]
		rhs, err := strconv.ParseInt(condStr[indexOfOperator+1:], 10, 64)
		if err != nil {
			fmt.Printf("Unable to parse integer from %s\n", lhsString)
			os.Exit(-1)
		}
		var sel LhsSelector
		sel = XSelector{}
		if lhsString == "m" {
			sel = MSelector{}
		} else if lhsString == "a" {
			sel = ASelector{}
		} else if lhsString == "s" {
			sel = SSelector{}
		}
		return Condition{
			t:           condType,
			lhsSelector: sel,
			rhs:         int(rhs),
			trueLabel:   trueLabel,
		}
	}
}

func (r Rule) applies(part Part, otherRules map[string]Rule) bool {
	currRule := r
	for {
		for _, subRule := range currRule {
			result, s := subRule.applies(part)
			if s != nil {
				currRule = otherRules[*s]
				break
			}
			if result == True {
				return true
			}
			if result == False {
				return false
			}
		}
	}
}

func (c Condition) applies(part Part) (CondResult, *string) {
	lhs := c.lhsSelector.selectPart(part)
	if ((c.t == LessThan) && lhs < c.rhs) || ((c.t == GreaterThan) && lhs > c.rhs) {
		return Unknown, &c.trueLabel
	}
	return Unknown, nil
}

func dfs(ongoingSolution Solution, ruleName string, allRules map[string]Rule) int {
	rule := allRules[ruleName]
	total := 0
	for _, subRule := range rule {
		switch sr := subRule.(type) {
		case Accept:
			return total + ongoingSolution.possibilities()
		case Reject:
			return total
		case Fallthrough:
			return total + dfs(ongoingSolution, sr.label, allRules)
		case Condition:
			trueCase := sr.lhsSelector.selectConditionTrue(sr.t, sr.rhs, ongoingSolution)
			total += dfs(trueCase, sr.trueLabel, allRules)
			ongoingSolution = sr.lhsSelector.selectConditionFalse(sr.t, sr.rhs, ongoingSolution)
		}
	}
	return total
}

func main() {
	content, err := os.ReadFile("./input.txt")
	if err != nil {
		fmt.Printf("Could not read file due to %s\n", err.Error())
		os.Exit(-1)
	}
	contentAsString := string(content)
	splitContent := strings.Split(contentAsString, "\n\n")
	if len(splitContent) != 2 {
		fmt.Printf("NOPENOPENOPENOPE\n")
		os.Exit(-1)
	}
	workflowsAsString := splitContent[0]
	ratingsAsString := splitContent[1]
	workflowRegexp := regexp.MustCompile(`(?P<name>[a-z]+){(?P<name>[^{]+)}`)
	partRegexp := regexp.MustCompile(`{x=(?P<x>-?[0-9]+),m=(?P<m>-?[0-9]+),a=(?P<a>-?[0-9]+),s=(?P<s>-?[0-9]+)}`)
	allRules := make(map[string]Rule)
	allRules["A"] = []SubRule{Accept{}}
	allRules["R"] = []SubRule{Reject{}}
	firstRuleName := "in"
	for _, workflow := range strings.Split(workflowsAsString, "\n") {
		match := workflowRegexp.FindStringSubmatch(workflow)
		name := match[1]
		rulesStr := match[2]
		rules := []SubRule{}
		for _, subrule := range strings.Split(rulesStr, ",") {
			rule := parseSubRule(subrule)
			rules = append(rules, rule)
		}
		allRules[name] = rules
	}
	totalRating := 0
	for _, partStr := range strings.Split(ratingsAsString, "\n") {
		match := partRegexp.FindStringSubmatch(partStr)
		// Ignore errors with impunity, because fuck you that's why.
		x, _ := strconv.Atoi(match[1])
		m, _ := strconv.Atoi(match[2])
		a, _ := strconv.Atoi(match[3])
		s, _ := strconv.Atoi(match[4])
		part := Part{
			x: x,
			m: m,
			a: a,
			s: s,
		}
		if allRules[firstRuleName].applies(part, allRules) {
			totalRating += part.rating()
		}
	}
	fmt.Printf("%d\n", totalRating)
	fmt.Printf("%d\n", dfs(newFullSolution(), "in", allRules))
}
