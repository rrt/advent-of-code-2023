Dim line_txt As String
Dim seeds(1024) As _Integer64
Dim new_seeds(1024) As _Integer64

Dim dst_range_start(1024) As _Integer64
Dim src_range_start(1024) As _Integer64
Dim range_lens(1024) As Long


Open "input.txt" For Input As #1
Input #1, line_txt
idx = first_ch_in_str(line_txt, ":")
line_txt = _Trim$(Mid$(line_txt, idx + 1))

num_seeds = 0
'Read seeds
While idx <> -1
    idx = first_ch_in_str(line_txt, " ")
    number_str$ = ""
    If idx <> -1 Then
        number_str$ = _Trim$(Mid$(line_txt, 1, idx))
        line_txt = _Trim$(Mid$(line_txt, idx + 1))
    Else
        number_str$ = line_txt
    End If
    number&& = Val(number_str$)
    num_seeds = num_seeds + 1
    seeds(num_seeds) = number&&
Wend
'ignore whitespace line
Input #1, line_txt
Cls
While Not EOF(1)
    'ignore line with mapping type (assume they're in a decent order)
    Input #1, line_txt
    line_txt = _Trim$(line_txt)
    num_mappings = 0

    While Len(line_txt) > 0 And Not EOF(1)
        Input #1, line_txt
        line_txt = _Trim$(line_txt)
        If Len(line_txt) > 0 Then
            num_mappings = num_mappings + 1
            idx = first_ch_in_str(line_txt, " ")
            dst_range$ = _Trim$(Mid$(line_txt, 1, idx - 1))
            idx_prev = idx
            idx = first_ch_in_str(Mid$(line_txt, idx + 1), " ")
            src_range$ = _Trim$(Mid$(line_txt, idx_prev + 1, idx - 1))
            range_len$ = _Trim$(Mid$(line_txt, idx_prev + idx + 1))
            dst_range_start(num_mappings) = Val(dst_range$)
            src_range_start(num_mappings) = Val(src_range$)
            range_lens(num_mappings) = Val(range_len$)
        End If
    Wend
    For i = 1 To num_seeds
        curr_seed&& = seeds(i)
        new_seed&& = curr_seed&&
        For j = 1 To num_mappings
            If curr_seed&& >= src_range_start(j) And curr_seed&& <= (src_range_start(j) + range_lens(j) - 1) Then
                new_seed&& = curr_seed&& - src_range_start(j) + dst_range_start(j)
                Exit For
            End If
        Next
        seeds(i) = new_seed&&
    Next
Wend

Close #1

min_seed&& = seeds(1)

For i = 1 To num_seeds
    If seeds(i) < min_seed&& Then
        min_seed&& = seeds(i)
    End If
Next

Print Str$(min_seed&&)

Function first_ch_in_str (str_val As String, ch As String)
    first_ch_in_str = -1
    For idx = 0 To Len(str_val) - 1
        If Mid$(str_val, idx, 1) = ch Then
            first_ch_in_str = idx
            Exit For
        End If
    Next
End Function
